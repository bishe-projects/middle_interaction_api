package client

import (
	"github.com/cloudwego/kitex/client"
	"github.com/cloudwego/kitex/transport"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction/interactionservice"
)

var (
	InteractionClient = interactionservice.MustNewClient("interaction_service", client.WithHostPorts("127.0.0.1:8888"), client.WithTransportProtocol(transport.TTHeaderFramed))
)
