package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/common_utils/middleware"
	"gitlab.com/bishe-projects/middle_interaction_api/handler"
)

func main() {
	r := gin.Default()
	r.Use(middleware.Cors(), middleware.VerifyTokenMiddleware())
	interactiveEntity := r.Group("/interactiveEntity")
	{
		interactiveEntity.POST("/create", handler.CreateInteractiveEntity)
		interactiveEntity.POST("/all", handler.AllInteractiveEntity)
	}
	interactiveAction := r.Group("/interactiveAction")
	{
		interactiveAction.POST("/create", handler.CreateInteractiveAction)
		interactiveAction.POST("/all", handler.AllInteractiveAction)
	}
	interaction := r.Group("/interaction")
	{
		interaction.POST("/create", handler.CreateInteraction)
		interaction.POST("/remove", handler.RemoveInteraction)
		interaction.POST("/getList", handler.GetInteractionList)
	}
	r.Run("0.0.0.0:8085")
}
