package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"

type CreateInteractiveAction struct {
	BizId int64  `json:"biz_id" binding:"required"`
	Name  string `json:"name" binding:"required"`
	Desc  string `json:"desc" binding:"required"`
}

func (dto *CreateInteractiveAction) ConvertToReq() *interaction.CreateActionReq {
	return &interaction.CreateActionReq{
		BizId: dto.BizId,
		Name:  dto.Name,
		Desc:  dto.Desc,
	}
}
