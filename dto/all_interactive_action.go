package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"

type AllInteractiveAction struct {
	BizId *int64 `json:"biz_id,omitempty"`
}

func (dto *AllInteractiveAction) ConvertToReq() *interaction.AllActionListReq {
	return &interaction.AllActionListReq{
		BizId: dto.BizId,
	}
}
