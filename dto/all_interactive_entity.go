package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"

type AllInteractiveEntity struct {
	BizId *int64 `json:"biz_id,omitempty"`
}

func (dto *AllInteractiveEntity) ConvertToReq() *interaction.AllEntityListReq {
	return &interaction.AllEntityListReq{
		BizId: dto.BizId,
	}
}
