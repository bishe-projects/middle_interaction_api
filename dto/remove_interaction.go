package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"

type RemoveInteraction struct {
	Interaction Interaction `json:"interaction" binding:"required"`
}

func (dto *RemoveInteraction) ConvertToReq() *interaction.RemoveInteractionReq {
	return &interaction.RemoveInteractionReq{
		Interaction: dto.Interaction.ConvertToInteraction(),
	}
}
