package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"

type Interaction struct {
	BizId             int64 `json:"biz_id" binding:"required"`
	ActionId          int64 `json:"action_id" binding:"required"`
	InitiatorEntityId int64 `json:"initiator_entity_id" binding:"required"`
	InitiatorId       int64 `json:"initiator_id" binding:"required"`
	AcceptorEntityId  int64 `json:"acceptor_entity_id" binding:"required"`
	AcceptorId        int64 `json:"acceptor_id" binding:"required"`
}

func (dto *Interaction) ConvertToInteraction() *interaction.Interaction {
	return &interaction.Interaction{
		BizId:             dto.BizId,
		ActionId:          dto.ActionId,
		InitiatorEntityId: dto.InitiatorEntityId,
		InitiatorId:       dto.InitiatorId,
		AcceptorEntityId:  dto.AcceptorEntityId,
		AcceptorId:        dto.AcceptorId,
	}
}
