package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"

type GetInteractionList struct {
	BizId             int64  `json:"biz_id" binding:"required"`
	ActionId          int64  `json:"action_id" binding:"required"`
	InitiatorEntityId int64  `json:"initiator_entity_id" binding:"required"`
	AcceptorEntityId  int64  `json:"acceptor_entity_id" binding:"required"`
	InitiatorId       *int64 `json:"initiator_id,omitempty"`
	AcceptorId        *int64 `json:"acceptor_id,omitempty"`
}

func (dto *GetInteractionList) ConvertToReq() *interaction.GetInteractionListReq {
	return &interaction.GetInteractionListReq{
		BizId:             dto.BizId,
		ActionId:          dto.ActionId,
		InitiatorEntityId: dto.InitiatorEntityId,
		AcceptorEntityId:  dto.AcceptorEntityId,
		InitiatorId:       dto.InitiatorId,
		AcceptorId:        dto.AcceptorId,
	}
}
