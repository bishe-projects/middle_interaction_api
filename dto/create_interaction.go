package dto

import "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"

type CreateInteraction struct {
	Interaction Interaction `json:"interaction" binding:"required"`
}

func (dto *CreateInteraction) ConvertToReq() *interaction.CreateInteractionReq {
	return &interaction.CreateInteractionReq{
		Interaction: dto.Interaction.ConvertToInteraction(),
	}
}
