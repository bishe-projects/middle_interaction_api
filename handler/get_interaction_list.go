package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/middle_interaction_api/client"
	"gitlab.com/bishe-projects/middle_interaction_api/dto"
	"net/http"
)

func GetInteractionList(c *gin.Context) {
	var getInteractionList dto.GetInteractionList
	if err := c.BindJSON(&getInteractionList); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	ctxVal, _ := c.Get("context")
	ctx := ctxVal.(context.Context)
	resp, err := client.InteractionClient.GetInteractionList(ctx, getInteractionList.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}
