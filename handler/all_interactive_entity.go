package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/middle_interaction_api/client"
	"gitlab.com/bishe-projects/middle_interaction_api/dto"
	"net/http"
)

func AllInteractiveEntity(c *gin.Context) {
	var allInteractiveEntity dto.AllInteractiveEntity
	if err := c.BindJSON(&allInteractiveEntity); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	ctxVal, _ := c.Get("context")
	ctx := ctxVal.(context.Context)
	resp, err := client.InteractionClient.AllEntityList(ctx, allInteractiveEntity.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}
