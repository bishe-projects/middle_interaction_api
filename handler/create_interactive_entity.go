package handler

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/bishe-projects/middle_interaction_api/client"
	"gitlab.com/bishe-projects/middle_interaction_api/dto"
	"net/http"
)

func CreateInteractiveEntity(c *gin.Context) {
	var createInteractiveEntity dto.CreateInteractiveEntity
	if err := c.BindJSON(&createInteractiveEntity); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	ctxVal, _ := c.Get("context")
	ctx := ctxVal.(context.Context)
	resp, err := client.InteractionClient.CreateEntity(ctx, createInteractiveEntity.ConvertToReq())
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}
	c.JSON(http.StatusOK, resp)
}
